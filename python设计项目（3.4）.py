'''
NAME:20192429马一
PROJECT:python程序设计作业
DATE:3月4日
'''

# 仿射密码
print("已知：E(x)=(5x+8) mod 26函数，解密函数就是D(x) = 21(x - 8) mod 26 。")
print("要求：输入一个x，计算E(x)的值y。然后计算D(y)并判断x是否等于D（y）。")
x=int(input("请输入一个数："))
E=(5*x+8)%26
print("该数加密为：",E)
D=21*(E-8)%26
print("该数解密为：",D)

#等比数列
print("当q≠1时,Sn=a[(q^n)-1]/(q-1) (n=1,2,3,...)")
a=int(input("请输入首项："))
n=int(input("请输入项数："))
q=int(input("请输入公比："))
if q==1:
    print("公比不能为1哦！")
if q !=1:
    Sn=int(a+(pow(q,n)-1)/(q-1))
    print("前n项和为：",Sn)