class Hero:
    '''王者英雄属性面板'''

    life = ""

    def __init__(self):
        self.life = ""

    def __init___(self, life = "3000hp"):
        Hero.life = life

    def initialize(self, life):
        print("英雄的初始生命值是：" + self.life)
        print("英雄升级了！")
        print("升级后的生命值是：" + Hero.life)


class Arthur(Hero):
    life = "2500hp"

    def __init__(self):
        print("亚瑟")
        super().__init___()


class Marco(Hero):
    def __init__(self, life):
        print("马可波罗")
        super().__init__()

    def initialize(self, life):
        print("英雄的生命加成是：" + life + "/Lv")
        print("英雄的最终生命值是：" + Hero.life)


arthur = Arthur()
arthur.initialize(arthur.life)
marco = Marco("120hp")
marco.initialize("120hp")
