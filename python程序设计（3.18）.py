tank = ["猪八戒","嫦娥","孙策","苏烈"]
soldier = ["曜","李信","狂铁","吕布"]
assasin = ["马超","云中君","韩信","李白"]
master = ["王昭君","小乔","沈梦溪","诸葛亮"]
shooter = ["孙尚香","马可波罗","成吉思汗","伽罗"]
assister = ["鲁班大师","瑶","明世隐","太乙真人"]
print("---坦克---\n",tank)
print("---战士---\n",soldier)
print("---刺客---\n",assasin)
print("---法师---\n",master)
print("---射手---\n",shooter)
print("---辅助---\n",assister)
newtank1 = ["牛魔"]
tank.extend(newtank1)
print("---增添后的坦克---\n",tank)
tank[2] = "白起"
print("---修改后的坦克---\n",tank)
del tank[-1]
print("---删除后的坦克---\n",tank)